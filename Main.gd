extends Control

export(Array, PackedScene) var stages

onready var current_stage_idx := 0
onready var stage: Stage = null

func _ready():
	#warning-ignore:return_value_discarded
	get_tree().connect('node_added', self, 'connect_stage_signals')
	visit(self)
	_restart_stage()

func visit(node: Node):
	connect_stage_signals(node)
	for child in node.get_children():
		visit(child)

func connect_stage_signals(node: Node):
	if node.has_signal('next_stage'):
		#warning-ignore:return_value_discarded
		node.connect('next_stage', self, '_next_stage')
	if node.has_signal('restart_stage'):
		#warning-ignore:return_value_discarded
		node.connect('restart_stage', self, '_restart_stage')
	if node.has_signal('message'):
		#warning-ignore:return_value_discarded
		node.connect('message', self, '_message')

func _next_stage():
	current_stage_idx += 1
	if current_stage_idx < stages.size():
		_restart_stage()
	else:
		$CanvasLayer/End.show()

func _input(event):
	if event.is_action_pressed("restart"):
		_restart_stage()

func _restart_stage():
	yield(get_tree(), 'physics_frame')
	if stage != null:
		remove_child(stage)
		stage.queue_free()
	stage = stages[current_stage_idx].instance()
	add_child(stage)

func _message(text: String):
	$CanvasLayer/Speech/MarginContainer/HBoxContainer/RichTextLabel.text = text
	$CanvasLayer/Speech.show()
	yield(get_tree().create_timer(3.0), "timeout")
	$CanvasLayer/Speech.hide()
