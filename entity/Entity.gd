class_name Entity extends KinematicBody2D

signal entity_ready(entity)

func get_water_physics():
	return $WaterPhysics

func _ready():
	propagate_call('_entity_ready', [self])
	emit_signal("entity_ready", self)

func _physics_process(delta: float):
	propagate_call('_entity_process', [self, delta])

func process_effect(name: String, effect: Dictionary = {}) -> Dictionary:
	propagate_call("_on_%s" % name, [self, effect])
	return effect

func _unhandled_input(event: InputEvent):
	if event.is_action_pressed("interact"):
		var effect = process_effect('interact', { success = false })
		if effect.success:
			pass # play a sound
	elif event.is_action_pressed('lower_body_action'):
		#warning-ignore:return_value_discarded
		process_effect('lower_body_action', { success = false })
	elif event.is_action_pressed('upper_body_action'):
		#warning-ignore:return_value_discarded
		process_effect('upper_body_action', { success = false })
