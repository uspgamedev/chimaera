extends Node2D

func _on_landed(_entity: Entity, effect: Dictionary):
	var victim := effect['on_entity'] as Entity
	#warning-ignore:return_value_discarded
	victim.process_effect('crush')

func _on_press(_entity: Entity, effect: Dictionary):
	effect['weight'] += 1
