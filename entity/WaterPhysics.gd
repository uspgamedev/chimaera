class_name WaterPhysics extends Area2D

export var damp := 0.8

onready var was_on_water := false

func _on_physics(_entity: Entity, effect: Dictionary):
	if get_overlapping_areas().size() + get_overlapping_bodies().size() > 0:
		effect['water_damp'] = damp
		effect['inside_water'] = true
		if not was_on_water:
			$SplashSFX.play()
		was_on_water = true
	else:
		was_on_water = false
