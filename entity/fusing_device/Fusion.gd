extends Node2D

export var part_scn: PackedScene

func _on_take(_entity: Entity, effect: Dictionary):
	effect.part_scn = part_scn

func _on_receive(_entity: Entity, effect: Dictionary):
	part_scn = effect.get('part_scn', null)
