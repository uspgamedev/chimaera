extends Node2D

export var sfx_scn: PackedScene

signal spawn(entity)

func _on_die(entity: Entity, _effect: Dictionary):
	var sfx := sfx_scn.instance() as Node2D
	sfx.position = entity.position
	emit_signal('spawn', sfx)
	entity.queue_free()
