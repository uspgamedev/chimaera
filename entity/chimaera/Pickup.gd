extends Area2D

export var is_upper_body := true

func _on_upper_body_action(entity: Entity, effects: Dictionary):
	if is_upper_body:
		return pickup(entity, effects)
	else:
		pass
		
func _on_lower_body_action(entity: Entity, effects: Dictionary):
	if not is_upper_body:
		return pickup(entity, effects)
	else:
		pass

func pickup(entity:Entity, effects: Dictionary):
	var bodies := get_overlapping_bodies()
	if bodies.empty():
		return effects
	var target: Entity = bodies.front()
	if target != null:
		#warning-ignore:return_value_discarded
		target.process_effect('pick_up', {})
		target.get_parent().remove_child(target)
		var socket := get_node("PickupSocket")
		target.set_position(Vector2(0,0))
		socket.add_child(target)
		#warning-ignore:return_value_discarded
		entity.process_effect('carry', {})
		return effects
