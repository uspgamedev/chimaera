extends Node2D

func _on_water_entered(entity: Entity, effect: Dictionary):
	if not effect.has('immune'):
		#warning-ignore:return_value_discarded
		entity.process_effect('die')
