extends AnimationPlayer

func _on_jump(_entity: Entity, _effect: Dictionary):
	play("jump")

func _on_physics(_entity: Entity, effect: Dictionary):
	if not effect.get('can_jump', false) and current_animation != 'jump':
		play("airborne")
		queue("idle")

func _on_move(_entity: Entity, effect: Dictionary):
	if current_animation in ['jump', 'airborne']:
		return
	var movement := effect['movement'] as Vector2
	if abs(movement.x) > 5:
		play("walk")
	else:
		play("idle")
