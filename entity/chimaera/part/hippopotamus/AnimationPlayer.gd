extends AnimationPlayer

onready var entity: Entity = null

func _on_upper_body_action(the_entity: Entity, _effect: Dictionary):
	play("bite")
	queue("idle")
	entity = the_entity

func bite():
	#warning-ignore:return_value_discarded
	entity.process_effect('bite')
	entity = null
