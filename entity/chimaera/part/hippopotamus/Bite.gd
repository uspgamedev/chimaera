extends Area2D

func _on_bite(_entity: Entity, _effect: Dictionary):
	var bodies = get_overlapping_bodies()
	if bodies.empty():
		return
	var target: Entity = bodies.front()
	if target != null:
		#warning-ignore:return_value_discarded
		target.process_effect('crush')
