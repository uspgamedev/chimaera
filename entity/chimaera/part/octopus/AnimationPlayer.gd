extends AnimationPlayer

func _on_jump(_entity: Entity, _effect: Dictionary):
	play("swim")
	queue("idle")
	
func _on_lower_body_action(_entity: Entity, _effect: Dictionary):
	play("grab")
	queue("idle")

func _on_carry(_entity: Entity, _effect: Dictionary):
	play("carrying")

func grab():
	pass
