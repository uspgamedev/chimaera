extends Node2D

func _on_physics(_entity: Entity, effect: Dictionary):
	if not effect.get('inside_water', false):
		effect["jump_str"] = 0
