class_name UpperPart extends Node2D

export var height := 8

func attach_breathing(water_physics: Node2D):
	$BreathingOffset.remote_path = water_physics.get_path()

func attach_collision(upper_shape: CollisionShape2D):
	upper_shape.shape.height = height
	$CollisionOffset.remote_path = upper_shape.get_path()
