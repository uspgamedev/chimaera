extends Node2D

export var min_angle := -45
export var max_angle := 45

onready var pointer_angle := $Pointer.rotation as float

func _process(_delta):
	var mouse_pos := get_viewport().get_mouse_position()
	var diff := mouse_pos - global_position
	diff.x *= global_scale.y #FIXME why?
	var angle := diff.angle()
	angle = clamp(angle, deg2rad(min_angle), deg2rad(max_angle))
	rotation = angle * scale.x
