extends Node2D

enum State {
	IDLE, SHOOTING, STUCK
}

export var web_speed := 1000
export var pull_force := 0.05
export var pull_threshold := 5

onready var state: int = State.IDLE
onready var shooting_dir := Vector2.ZERO
onready var stuck_pos := Vector2.ZERO

func _on_lower_body_action(_entity: Entity, _effect: Dictionary):
	state = State.SHOOTING
	shooting_dir = Vector2.RIGHT.rotated($Aim.global_rotation)
	$ShootSFX.play()

func _on_physics(_entity: Entity, effect: Dictionary):
	if not Input.is_action_pressed("lower_body_action"):
		state = State.IDLE
	match state:
		State.IDLE:
			$Web.points[1] = Vector2.ZERO
			$Tip.position = Vector2.ZERO
			$RayCast.enabled = false
		State.SHOOTING:
			var movement := shooting_dir * web_speed * effect['delta'] as float
			var result := ($Tip as KinematicBody2D).move_and_collide(movement)
			if result != null:
				state = State.STUCK
				stuck_pos = $Tip.global_position
				$Tip.position = Vector2.ZERO
				$RayCast.enabled = true
			$Web.points[1] = $Tip.position
		State.STUCK:
			var target_pos := global_transform.xform_inv(stuck_pos) as Vector2
			$Web.points[1] = target_pos
			$RayCast.cast_to = target_pos - target_pos.normalized() * pull_threshold
			var diff := stuck_pos - global_position
			effect['pulling'] = true
			if diff.length_squared() > pull_threshold * pull_threshold:
				effect['pull_force'] = diff * pull_force
			if $RayCast.is_colliding():
				state = State.IDLE

func _process(_delta):
	if state != State.IDLE:
		$Web.show()
	else:
		$Web.hide()
