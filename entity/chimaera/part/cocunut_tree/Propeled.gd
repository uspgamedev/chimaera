extends Node2D

export var speed := 500
export var crash_sfx_scn: PackedScene

onready var dir := Vector2.ZERO

signal spawn(node)

func _on_propel(_entity: Entity, effect: Dictionary):
	dir = effect.get('dir') as Vector2

func _entity_process(entity: Entity, delta: float):
	var result := entity.move_and_collide(dir * speed * delta)
	if result:
		var collider = result.collider
		if collider is Entity:
			collider.process_effect('crush')
		var sfx := crash_sfx_scn.instance() as Node2D
		sfx.position = entity.position
		emit_signal("spawn", sfx)
		entity.queue_free()
		
