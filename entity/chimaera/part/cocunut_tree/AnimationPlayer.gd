extends AnimationPlayer

onready var entity: Entity

func _on_upper_body_action(the_entity: Entity, _effect: Dictionary):
	if current_animation == 'idle':
		play("shoot")
		queue("idle")
		entity = the_entity

func shoot():
	#warning-ignore:return_value_discarded
	entity.process_effect('shoot')
	entity = null
