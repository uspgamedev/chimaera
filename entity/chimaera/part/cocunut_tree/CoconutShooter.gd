extends Node2D

export var coconut_scn: PackedScene

signal spawn(entity)

func _on_shoot(_entity: Entity, _effect: Dictionary):
	var coconut := coconut_scn.instance()
	coconut.position = global_position
	#warning-ignore:return_value_discarded
	coconut.connect('entity_ready', self, '_coconut_ready')
	emit_signal("spawn", coconut)
	$ShootSFX.play()

func _coconut_ready(entity: Entity):
	var shooting_dir = Vector2.RIGHT.rotated($Aim.global_rotation)
	#warning-ignore:return_value_discarded
	entity.process_effect('propel', { dir = shooting_dir })
