extends Node2D

export var air_damp := 0.4
export var glide_threshold := 0.66
export var animation_path := NodePath()

onready var last_height := 0.0

func _on_physics(entity: Entity, effect: Dictionary):
	var height := entity.position.y
	var animation := get_node(animation_path) as AnimationPlayer
	if height - last_height > glide_threshold and Input.is_action_pressed("move_up"):
		effect['air_damp'] = air_damp
		animation.play('glide')
	else:
		animation.queue('idle')
	last_height = height
