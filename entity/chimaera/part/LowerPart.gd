class_name LowerPart extends Node2D

func attach(part: UpperPart):
	while not is_inside_tree() or not part.is_inside_tree():
		yield(get_tree(), "physics_frame")
	$UpperPartOffset.remote_path = part.get_path()
