extends Node

export var jump_str_modifier = 2.0

func _on_physics(_entity: Entity, effect: Dictionary):
	effect["jump_str"] = jump_str_modifier * 100
	return effect
	
