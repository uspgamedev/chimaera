extends Area2D

func _on_kick(entity: Entity, _effect: Dictionary):
	var bodies = get_overlapping_bodies()
	if bodies.empty():
		return
	var target: Entity = bodies.front()
	if target != null:
		var dir := sign(target.position.x - entity.position.x)
		#warning-ignore:return_value_discarded
		target.process_effect('push', { dir = dir })
