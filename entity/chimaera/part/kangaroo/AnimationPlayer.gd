extends AnimationPlayer

onready var entity: Entity = null

func _on_physics(_entity: Entity, effect: Dictionary):
	if current_animation == 'kick' and is_playing():
		return
	if not effect['can_jump']:
		play("airborne")
	else:
		play('idle')

func _on_lower_body_action(the_entity: Entity, _effect: Dictionary):
	entity = the_entity
	play("kick")

func kick():
	#warning-ignore:return_value_discarded
	entity.process_effect('kick')
	entity = null
