class_name Multipart extends Node2D

export var upper_shape_path := NodePath()
export var breathing_path := NodePath()

onready var lower_part: LowerPart = null
onready var upper_part: UpperPart = null

func _entity_ready(_entity: Entity):
	for child in get_children():
		if child is LowerPart:
			fuse_part(child)
	assert(lower_part != null)
	for child in get_children():
		if child is UpperPart:
			fuse_part(child)

func _on_interact(_entity: Entity, effect: Dictionary):
	var target: Entity = effect.get("target")
	if target != null:
		var change_effect: Dictionary = target.process_effect('take')
		var part_scn: PackedScene = change_effect.get('part_scn')
		if part_scn != null:
			fuse_part(part_scn.instance(), target)
			$SwapSFX.play()
			effect.success = true

func fuse_part(part: Node2D, source: Entity = null):
	if part is LowerPart:
		if lower_part != null:
			#warning-ignore:return_value_discarded
			source.process_effect('receive', { part_scn = load(lower_part.filename) })
			lower_part.queue_free()
		lower_part = part
	elif part is UpperPart:
		if upper_part != null:
			#warning-ignore:return_value_discarded
			source.process_effect('receive', { part_scn = load(upper_part.filename) })
			upper_part.queue_free()
		upper_part = part
	else:
		return
	if not is_a_parent_of(part):
		call_deferred('add_child', part)
	call_deferred('_attach_parts')

func _attach_parts():
	if upper_part != null and lower_part != null:
		lower_part.attach(upper_part)
	if upper_part != null:
		upper_part.attach_breathing(get_node(breathing_path))
		upper_part.attach_collision(get_node(upper_shape_path))
