extends Area2D

func _on_physics(entity: Entity, effect: Dictionary):
	if 		get_overlapping_areas().size() + get_overlapping_bodies().size() > 0 \
		and	effect.get('inside_water'):
			#warning-ignore:return_value_discarded
			entity.process_effect('water_entered')

