extends Area2D

func _on_interact(_entity: Entity, effect: Dictionary):
	var bodies = get_overlapping_bodies()
	if not bodies.empty():
		effect.target = bodies.front()

func _on_Interaction_body_entered(body):
	print(body)
