extends Node

export var gravity := 10
export var acceleration := 10
export var max_speed := 200
export var floor_damp := 0.8
export var jump_str := 100

onready var movement := Vector2.ZERO

func _entity_process(entity: Entity, delta: float):
	var physics_effect := entity.process_effect('physics', {
		delta = delta,
		can_jump = can_jump()
	})
	var pull := physics_effect.get('pull_force', Vector2.ZERO) as Vector2
	var pulling := physics_effect.get('pulling', false) as bool
	var walk_speed := walk_force() * delta
	var inside_water := physics_effect.get('inside_water', false) as bool
	if walk_speed.x > 0:
		entity.transform.x.x = 1
	elif walk_speed.x < 0:
		entity.transform.x.x = -1
	if not pulling:
		movement += walk_speed
	
	var jump_force := 0.0
	if (can_jump() or inside_water) and Input.is_action_just_pressed('move_up'):
		jump_force = Vector2.UP.y * physics_effect.get('jump_str', jump_str)
	elif can_jump() and physics_effect.get('force_jump', false) and (horizontal_control() != 0):
		jump_force = Vector2.UP.y * (physics_effect.get('jump_str', jump_str) if Input.is_action_pressed("move_up") else jump_str)
	elif not entity.is_on_floor() and not pulling:
		movement += Vector2.DOWN * gravity * delta
	if abs(jump_force) > 0:
		movement.y = jump_force
		notify_jump(entity)
	movement.x = clamp(movement.x, -max_speed, max_speed)
	movement += pull
	#warning-ignore:return_value_discarded
	entity.process_effect('move', { movement = movement })
	movement = entity.move_and_slide(movement, Vector2.UP)
	if pulling:
		if pull.length_squared() <= 0:
			movement = Vector2.ZERO
	else:
		movement.x *= floor_damp
	movement.y *= physics_effect.get('air_damp', 1.0)
	movement *= physics_effect.get('water_damp', 1.0)
	for i in entity.get_slide_count():
		var collision := entity.get_slide_collision(i)
		var collider = collision.collider
		if collider is Entity:
			#warning-ignore:return_value_discarded
			entity.process_effect('landed', { on_entity = collider })

func _on_press(_entity: Entity, effect: Dictionary):
	effect['weight'] += 1

func walk_force() -> Vector2:
	var control := Vector2.RIGHT * horizontal_control()
	control *= acceleration
	return control

func can_jump() -> bool:
	return not $BunnyHopHitbox.get_overlapping_bodies().empty()

func notify_jump(entity: Entity):
	$JumpSFX.play()
	#warning-ignore:return_value_discarded
	entity.process_effect('jump')

func horizontal_control() -> float:
	return Input.get_action_strength("move_right") \
		 - Input.get_action_strength("move_left")
