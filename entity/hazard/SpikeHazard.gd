extends Area2D

func _on_Area2D_body_entered(body):
	var victim: Entity = body
	victim.process_effect('die', {})
