extends Node2D

export var anim_path := NodePath()

onready var opening := false

func _on_open(entity: Entity, effect: Dictionary):
	if not opening:
		opening = true
		$OpenSFX.play()
		get_node(anim_path).play('opening')
		yield($OpenSFX, "finished")
		entity.queue_free()
