extends Area2D

export var locked:= 0

func _on_KeyHole_body_entered(body: Entity):
	var effect := body.process_effect('unlock')
	if effect.get('unlock') == locked:
		get_parent().process_effect('open', effect)
		body.process_effect('used')
