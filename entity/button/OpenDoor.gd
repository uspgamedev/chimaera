extends Node2D

export (NodePath) var triggerable

func _on_trigger(_entity: Entity, _effect: Dictionary):
	var target: Entity = get_node_or_null(triggerable)
	if target:
		#warning-ignore:return_value_discarded
		target.process_effect('open')
