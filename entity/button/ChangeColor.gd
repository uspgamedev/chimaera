extends Polygon2D

export (Color) var new_color

var triggered := false

func _on_triggered(_entity: Entity, _effect: Dictionary):
	if not triggered:
		set_color(Color( 1, 1, 1, 1 ) if triggered else new_color)
		triggered = true
