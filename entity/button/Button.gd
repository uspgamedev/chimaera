extends Area2D

export (NodePath) var topview
export (NodePath) var topcollision

onready var pressed := false

func _entity_process(entity: Entity, _delta: float):
	if pressed: return
	for body in get_overlapping_bodies():
		if body is Entity:
			var effect := body.process_effect('press', { weight = 0 }) as Dictionary
			if effect['weight'] > 0:
				
				pressed = true
				get_node(topview).frame = 1
				get_node(topcollision).set_deferred('disabled', true)
				$ClickSFX.play()
				yield($ClickSFX, "finished")
				#warning-ignore:return_value_discarded
				entity.process_effect('trigger')
				break
