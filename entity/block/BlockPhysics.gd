extends Node2D

export var gravity := 10
export var max_speed := 200
export var floor_damp := 0.8

onready var movement := Vector2.ZERO

func _entity_process(entity: Entity, delta: float):
	var physics_effect := entity.process_effect('physics', {})
	movement.x += physics_effect.get('push_movement', 0.0)
	if not entity.is_on_floor():
		movement += Vector2.DOWN * gravity * delta
	movement.x = clamp(movement.x, -max_speed, max_speed)
	movement = entity.move_and_slide(movement, Vector2.UP)
	movement.x *= floor_damp
	movement *= physics_effect.get('air_damp', 1.0)
	for i in entity.get_slide_count():
		var collision := entity.get_slide_collision(i)
		var collider = collision.collider
		if collider is Entity:
			#warning-ignore:return_value_discarded
			entity.process_effect('landed', { on_entity = collider })

func _on_press(_entity: Entity, effect: Dictionary):
	effect['weight'] += 1
