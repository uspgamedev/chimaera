extends Node2D

export var sfx_scn: PackedScene

signal spawn(node)

func _on_crush(entity: Entity, _effect: Dictionary):
	var sfx := sfx_scn.instance() as Node2D
	sfx.position = entity.position
	emit_signal("spawn", sfx)
	entity.queue_free()
	
