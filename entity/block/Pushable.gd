extends Node2D

export var push_force := 100
onready var push := 0.0

func _on_push(_entity: Entity, effect: Dictionary):
	push = effect.get('dir', 0.0)
	effect.success = true
	$PushSFX.play()

func _on_physics(_entity: Entity, effect: Dictionary):
	effect.push_movement = push * push_force
	push = 0.0
