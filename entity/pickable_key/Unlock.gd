extends Node2D
export var unlock := 1
	
func _on_unlock(entity: Entity, effect: Dictionary):
	effect['unlock'] = unlock
	return effect

func _on_used(entity: Entity, effect: Dictionary):
	entity.queue_free()
