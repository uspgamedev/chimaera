class_name Stage extends Node2D

onready var player_died := false

signal restart_stage

func _ready():
	#warning-ignore:return_value_discarded
	get_tree().connect('node_added', self, 'connect_spawn_signal')
	visit(self)

func visit(node: Node):
	connect_spawn_signal(node)
	for child in node.get_children():
		visit(child)

func connect_spawn_signal(node: Node):
	if node.has_signal('spawn'):
		#warning-ignore:return_value_discarded
		node.connect('spawn', self, '_spawn')

func _spawn(node: Node2D):
	add_child(node)

func _physics_process(_delta):
	if not player_died and get_tree().get_nodes_in_group('player').empty():
		player_died = true
		yield(get_tree().create_timer(1.0), "timeout")
		emit_signal("restart_stage")
