extends Node2D

export(String, MULTILINE) var text

onready var seen := false

signal message(text)

func notify_message(_body):
	if not seen:
		seen = true
		emit_signal("message", text)
