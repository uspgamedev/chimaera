extends Area2D

signal next_stage

func _on_body_entered(body):
	if body is Entity and body.is_in_group('player'):
		emit_signal("next_stage")
