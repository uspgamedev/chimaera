extends Node2D

export var delay := 1.0
export(String, MULTILINE) var text

signal message(text)

# Called when the node enters the scene tree for the first time.
func _ready():
	$Timer.start(delay)

func notify_message():
	emit_signal("message", text)
